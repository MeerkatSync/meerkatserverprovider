import XCTest

import MeerkatServerProviderTests

var tests = [XCTestCaseEntry]()
tests += MeerkatServerProviderTests.allTests()
XCTMain(tests)
