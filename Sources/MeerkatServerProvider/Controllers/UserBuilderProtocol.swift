//
//  UserBuilderProtocol.swift
//  
//
//  Created by Filip Klembara on 22/04/2020.
//

public protocol UserBuilderProtocol {
    associatedtype UserTable: PrivateUserTable where UserTable.TokenType: Model, UserTable.TokenType.Database == UserTable.Database, UserTable.TokenType.UserIDType == UserTable.ID
    associatedtype PublicUser: Content
    associatedtype UserTokenAuth: Content
    associatedtype UserDevice: Device & Content

    var userTableId: KeyPath<UserTable, String> { get }

    func createUser(from: PublicUser, on: Container) throws -> Future<UserTable>

    func createToken(for: UserTable, on: Container) throws -> Future<UserTable.TokenType>

    func createToken(for: UserTokenAuth, on: Container) throws -> Future<UserTable.TokenType>

    var createDefaultGroup: Bool { get }
}

extension UserBuilderProtocol {
    public var createDefaultGroup: Bool { true }

    public func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
