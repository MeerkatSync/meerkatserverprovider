//
//  SyncController.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

open class SyncController<UserTable: PrivateUserTable>: RouteRegistrable {
    open func patch(req: Request) throws -> Future<CodableDiff> {
        let logger = try req.make(Logger.self)
        logger.info("patch")
        let user = try req.requireAuthenticated(UserTable.self)
        let id = try req.parameters.next(String.self)
        guard user.syncId == id else {
            throw Abort(.unauthorized)
        }
        let json = try req.content.decode(json: CodablePush.self, using: JSONDecoder())
        let sync = try req.make(ServerSyncController.self)
        let scheme = try req.make(MeerkatSchemeDescriptor.self)
        let getUser = sync.getUser(by: user.syncId)
        let transaction = json.map { try SyncTransaction(from: $0.transaction, scheme: scheme) }
        let groups = json.map { $0.groups.map { SyncGroup(id: $0.id, version: $0.version) } }
        let push = getUser.and(transaction.and(groups)).flatMap {
            sync.push($1.0, into: $1.1, by: $0)
        }

        let cod = push.map { try CodableDiff(from: $0, scheme: scheme)}

        return cod
    }
    
    open func pull(req: Request) throws -> Future<CodableDiff> {
        let logger = try req.make(Logger.self)
        logger.info("pull request")
        let user = try req.requireAuthenticated(UserTable.self)
        let id = try req.parameters.next(String.self)
        guard user.syncId == id else {
            throw Abort(.unauthorized)
        }
        let json = try req.content.decode(json: CodablePull.self, using: JSONDecoder())
        let sync = try req.make(ServerSyncController.self)
        let scheme = try req.make(MeerkatSchemeDescriptor.self)
        let getUser = sync.getUser(by: user.syncId)
        let groups = json.map { $0.groups.map { SyncGroup(id: $0.id, version: $0.version) } }
        let pull = groups.and(getUser).flatMap { sync.pull(from: $0, by: $1) }
        let cod = pull.map { try CodableDiff(from: $0, scheme: scheme)}
        return cod
    }

    open func register(on router: Router) {
        router.grouped(UserTable.tokenAuthMiddleware()).group("sync", String.parameter) { router in
            router.patch(use: patch)
            router.post(use: pull)
        }
    }
}

struct SyncGroup: Group, Content {
    let id: GroupID

    let version: GroupVersion
}
