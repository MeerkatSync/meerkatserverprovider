//
//  DefaultUserController.swift
//  
//
//  Created by Filip Klembara on 22/04/2020.
//

open class DefaultUserController<Builder: UserBuilderProtocol>: RouteRegistrable {
    public let builder: Builder
    public typealias TokenTable = Builder.UserTable.TokenType
    public typealias UserTable = Builder.UserTable

    public init(builder: Builder) {
        self.builder = builder
    }

    // create new user, syncid and group
    open func create(_ req: Request) throws -> Future<SyncIDTokenResponseBody> {
        let logger = try req.make(Logger.self)
        logger.info("create user")
        let user = try req.content.decode(Builder.PublicUser.self)
        let createUser = { user.flatMap { try self.builder.createUser(from: $0, on: req) } }
        let createToken = { (user: UserTable) -> Future<TokenTable> in
            try self.builder.createToken(for: user, on: req)
        }

        let sync = try req.make(ServerSyncController.self)
        let registration = createUser().flatMap { u in try createToken(u).map { (u, $0.bearerToken) } }
        let createSyncUser: () -> Future<SyncIDTokenResponseBody> = {
            registration.flatMap { user, token -> Future<SyncIDTokenResponseBody> in
                sync.createUser(with: user.syncId).flatMap { u in
                    let res: Future<SyncIDTokenResponseBody>
                    let r = SyncIDTokenResponseBody(syncId: user.syncId, token: token)
                    if self.builder.createDefaultGroup {
                        return sync.createGroup(with: user.syncId, objects: [], by: u).transform(to: r)
                    } else {
                        res = req.future(r)
                    }
                    return res
                }
            }
        }
        return createSyncUser()
    }

    // creates new token
    open func createToken(_ req: Request) throws -> Future<TokenResponseBody> {
        let logger = try req.make(Logger.self)
        logger.info("create token")
        let user = try req.content.decode(Builder.UserTokenAuth.self)
        let newToken = user.flatMap { try self.builder.createToken(for: $0, on: req) }
        let transformToken = { newToken.map { TokenResponseBody(token: $0.bearerToken) } }
        return transformToken()
    }

    // remove user
    open func delete(_ req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("create delete")
        let user = try req.requireAuthenticated(Builder.UserTable.self)

        let id = try req.parameters.next(String.self)

        guard user[keyPath: builder.userTableId] == id else {
            throw Abort(.unauthorized)
        }

        let removeSync = { (id: String) -> EventLoopFuture<Void> in
            let sync = try req.make(ServerSyncController.self)
            return sync.removeUser(by: id)
        }
        let deleteTokens: () -> Future<Void> = {
            req.withPooledConnection(to: .mysql) { conn -> Future<Void> in
                try user.authTokens.query(on: conn).delete()
            }
        }

        let deleteUser = { req.withPooledConnection(to: .mysql) { user.delete(on: $0) } }
        return try removeSync(user.syncId).flatMap { deleteTokens() }.flatMap { deleteUser() }.transform(to: .ok)
    }

    open func addDevice(_ req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("add device")
        let user = try req.requireAuthenticated(UserTable.self)

        let id = try req.parameters.next(String.self)

        guard user[keyPath: builder.userTableId] == id else {
            throw Abort(.unauthorized)
        }
        let sync = try req.make(ServerSyncController.self)
        let device = try req.content.decode(Builder.UserDevice.self)
        let db = try req.make(NotificatorDBWrapper.self)
        let getUser = sync.getUser(by: user.syncId)

        let delete = { (user: User) -> Future<User> in
            device.flatMap { db.remove(deviceId: $0.id, for: user) }.mapIfError { _ in }.transform(to: user)
        }
        let add = { (user: User) -> Future<Void> in
            device.flatMap { db.add(device: $0, for: user) }
        }
        return getUser.flatMap(delete).flatMap(add).transform(to: .ok)
    }

    open func removeDevice(_ req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("remove device")
        let user = try req.requireAuthenticated(UserTable.self)

        let id = try req.parameters.next(String.self)

        guard user[keyPath: builder.userTableId] == id else {
            throw Abort(.unauthorized)
        }
        let sync = try req.make(ServerSyncController.self)
        let deviceId = try req.parameters.next(String.self)
        let getUser = sync.getUser(by: user.syncId)
        let db = try req.make(NotificatorDBWrapper.self)
        let remove = getUser.flatMap { db.remove(deviceId: deviceId, for: $0) }
        return remove.transform(to: .ok)
    }

    // get sync id
    open func getSyncId(_ req: Request) throws -> SyncIDResponseBody {
        let logger = try req.make(Logger.self)
        logger.info("get sync id")
        let user = try req.requireAuthenticated(UserTable.self)

        let id = try req.parameters.next(String.self)

        guard user[keyPath: builder.userTableId] == id else {
            throw Abort(.unauthorized)
        }
        let token = user.syncId
        return SyncIDResponseBody(syncId: token)
    }

    open func register(on router: Router) {
        router.group("users") { router in
            router.post(use: create)
            router.post("tokens", use: createToken)
        }
        router.group(UserTable.tokenAuthMiddleware()) { router in
            router.group("users", String.parameter) { router in
                router.delete(use: delete)
                router.get("sync", use: getSyncId)
                router.group("devices") { router in
                    router.post(use: addDevice)
                    router.delete(String.parameter, use: removeDevice)
                }
            }
        }
    }
}

extension SyncIDResponseBody: Content { }

extension TokenResponseBody: Content { }

extension SyncIDTokenResponseBody: Content { }
