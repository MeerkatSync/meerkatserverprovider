//
//  GroupController.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

open class GroupController<UserTable: PrivateUserTable>: RouteRegistrable {
    public struct RoleInput: Codable {
        let role: Role
    }

    open func unsubscibe(req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("unsubscribe from group")
        let u = try req.requireAuthenticated(UserTable.self)
        let sync = try req.make(ServerSyncController.self)
        let groupId = try req.parameters.next(String.self)
        let oldUserId = try req.parameters.next(String.self)
        let getUser = { sync.getUser(by: u.syncId) }

        return getUser().flatMap { sync.unsubscribe(oldUserId, from: groupId, by: $0) }.transform(to: .ok)
    }

    open func subscribe(req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("subscribe to group")
        let u = try req.requireAuthenticated(UserTable.self)
        let sync = try req.make(ServerSyncController.self)
        let role = { try req.content.decode(RoleInput.self).map { $0.role } }
        let groupId = try req.parameters.next(String.self)
        let newUserId = try req.parameters.next(String.self)
        let getUser = { sync.getUser(by: u.syncId) }

        let roleUser = { try role().and(getUser()) }
        return try roleUser().flatMap { sync.subscribe(newUserId, into: groupId, by: $1, as: $0) }.transform(to: .created)
    }

    open func removeGroup(req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("delete group")
        let u = try req.requireAuthenticated(UserTable.self)
        let sync = try req.make(ServerSyncController.self)
        let groupId = try req.parameters.next(String.self)
        let user = sync.getUser(by: u.syncId)
        let deleteGroup: (Future<User>) -> Future<HTTPStatus> = { f in
            f.flatMap { sync.removeGroup(with: groupId, by: $0) }.transform(to: .ok)
        }
        return deleteGroup(user)
    }

    open func createGroup(req: Request) throws -> Future<HTTPStatus> {
        let logger = try req.make(Logger.self)
        logger.info("create group")
        let u = try req.requireAuthenticated(UserTable.self)

        let sync = try req.make(ServerSyncController.self)
        let groupId = try req.parameters.next(String.self)
        let user = sync.getUser(by: u.syncId)
        let createGroup: (Future<User>) -> Future<HTTPStatus> = { f in
            f.flatMap { sync.createGroup(with: groupId, objects: [], by: $0) }.transform(to: HTTPStatus.created)
        }
        return createGroup(user)
    }

    open func register(on router: Router) {
        let root = router.grouped(UserTable.tokenAuthMiddleware()).grouped("groups", String.parameter)
        root.group("subscribers", String.parameter) { (router) in
            router.delete(use: unsubscibe)
            router.post(use: subscribe)
        }
        root.delete(use: removeGroup)
        root.post(use: createGroup)
    }
}
