//
//  RouteRegistrable.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

public protocol RouteRegistrable {
    func register(on router: Router)
}

extension Router {
    public func register(_ regs: RouteRegistrable) {
        regs.register(on: self)
    }
}
