//
//  PublicGroupInfoController.swift
//  
//
//  Created by Filip Klembara on 22/04/2020.
//

import Fluent

open class PublicGroupInfoController<UserTable: PrivateUserTable, PublicUser: Content>: RouteRegistrable {
    public typealias UserMap = (UserTable, Role, String) -> PublicUser
    private let mapToPublicUser: UserMap
    private let userIdKeyPath: KeyPath<UserTable, String>

    public init(userIdKeyPath: KeyPath<UserTable, String>, mapToPublicUser: @escaping UserMap) {
        self.mapToPublicUser = mapToPublicUser
        self.userIdKeyPath = userIdKeyPath
    }

    open func getUsers(req: Request) throws -> Future<[PublicUser]> {
        let logger = try req.make(Logger.self)
        logger.info("get users in group")

        let user = try req.requireAuthenticated(UserTable.self)
        let sync = try req.make(ServerSyncController.self)
        let groupId = try req.parameters.next(String.self)
        let usr = sync.getUser(by: user.syncId)
        let usrs = usr.flatMap { sync.users(for: groupId, by: $0) }
        let roleMap = usrs.map { Dictionary($0.map { ($0.user.id, $0.role) }) { a, b in a } }
        let syncIds = usrs.map { $0.map { $0.user.id } }
        let privateUsers = syncIds.flatMap { UserTable.query(on: req).filter(\.syncId ~~ $0).all() }
        let result = privateUsers.and(roleMap).map { us, roles in
            us.compactMap { u -> PublicUser? in
                guard let role = roles[u.syncId] else {
                    return nil
                }
                return self.mapToPublicUser(u, role, groupId)
            }
        }
        return result
    }

    open func getSyncId(req: Request) throws -> Future<SyncIDResponseBody> {
        let logger = try req.make(Logger.self)
        logger.info("get sync id for user")

        let user = try req.requireAuthenticated(UserTable.self)
        let tokenUserId = try req.parameters.next(String.self)
        guard user[keyPath: userIdKeyPath] == tokenUserId else {
            throw Abort(.unauthorized)
        }
        let id = try req.parameters.next(String.self)
        let usr = UserTable.query(on: req).filter(userIdKeyPath == id).first().unwrap(or: Abort(.notFound))
        return usr.map { SyncIDResponseBody(syncId: $0.syncId) }
    }

    open func register(on router: Router) {
        let root = router.grouped(UserTable.tokenAuthMiddleware())
        root.get("groups", String.parameter, "subscribers", use: getUsers)
        root.get("public-users", String.parameter, "sync-id", String.parameter, use: getSyncId)
    }
}

