//
//  DBWrapperError+AbortError.swift
//  App
//
//  Created by Filip Klembara on 09/02/2020.
//

extension DBWrapperError: AbortError {
    public var status: HTTPResponseStatus {
        switch self {
        case .unknownUser, .unknownGroup:
            return .notFound
        case .wrongPermissions, .userNotInGroup:
            return .forbidden
        case .groupAlreadyExists, .userAlreadyExists:
            return .conflict
        case .databaseError:
            return .internalServerError
        case .unknownObject:
            return .badRequest
        }
    }
    public var identifier: String {
        "DBWrapperError"
    }

    public var reason: String {
        switch self {
        case .unknownUser(let id):
            return "Unknown user with id \(id)."
        case .unknownGroup(let id):
            return "Unknown group with id \(id)."
        case let .wrongPermissions(user, has, need):
            return "User with id \(user.id) has \(has) permissions but \(need) are needed."
        case .groupAlreadyExists(let id):
            return "Group with id \(id) already exists."
        case .userNotInGroup(let user, let group):
            return "User with id \(user.id) is not in group with id \(group.id)."
        case .databaseError(let err):
            return "Database error: \(err)."
        case .userAlreadyExists(let id):
            return "User with id \(id) already exists."
        case .unknownObject(let id):
            return "Unknown object with id \(id)"
        }
    }
}

