//
//  PrivateUserTable.swift
//  
//
//  Created by Filip Klembara on 10/03/2020.
//

public protocol PrivateUserTable: TokenAuthenticatable & Model {
    var syncId: String { get }
}
