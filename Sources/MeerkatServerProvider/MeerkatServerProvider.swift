//
//  MeerkatServerProvider.swift
//  MeerkatServerProvider
//
//  Created by Filip Klembara on 06/02/2020.
//

public struct MeerkatServerProvider<UserTable: PrivateUserTable> {
    public init(userType: UserTable.Type = UserTable.self) { }

    private let mySQLProvider = MeerkatMySQLProvider()
    
    public func setUpRoutes(_ router: Router) {
        let syncController = SyncController<UserTable>()
        syncController.register(on: router)
        let groupController = GroupController<UserTable>()
        groupController.register(on: router)
    }

    public func setUpMigration(_ migration: inout MigrationConfig) {
        mySQLProvider.setUpMigration(&migration)
    }
}

extension MeerkatServerProvider: Provider {
    public func register(_ services: inout Services) throws {
        let mySQLProvider = MeerkatMySQLProvider()
        try services.register(mySQLProvider)
        services.register(MeerkatServerSyncController.self)
    }

    public func didBoot(_ container: Container) throws -> EventLoopFuture<Void> {
        container.future()
    }
}
