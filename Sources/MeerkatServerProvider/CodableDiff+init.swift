//
//  CodableDiff+init.swift
//  
//
//  Created by Filip Klembara on 21/02/2020.
//

extension CodableDiff {
    init(from diff: Diff, scheme: MeerkatSchemeDescriptor) throws {
        let transactions = try diff.transactions.map { try CodableTransaction(from: $0, scheme: scheme) }
        let groups = diff.groups.map { CodableDiffGroupUpdate(from: $0) }
        self.init(transactions: transactions, groups: groups)
    }
}

extension CodableDiffGroupUpdate {
    init(from dgu: DiffGroupUpdate) {
        self.init(groupId: dgu.group.id, version: dgu.group.version, role: dgu.role)
    }
}

extension CodableTransaction {
    init(from tran: Transaction, scheme: MeerkatSchemeDescriptor) throws {
        try self.init(deletions: tran.deletions.map { try CodableSubtransaction(from: $0, scheme: scheme) },
                  creations: tran.creations.map { try CodableCreationDescription(from: $0, scheme: scheme) },
                  modifications: tran.modifications.map { try CodableSubtransaction(from: $0, scheme: scheme) })
    }
}

extension CodableSubtransaction {
    init(from sub: SubTransaction, scheme: MeerkatSchemeDescriptor) throws {
        try self.init(timestamp: sub.timestamp,
                      updates: sub.updates.map { try CodableUpdate(from: $0, className: sub.className, scheme: scheme)},
                      className: sub.className,
                      objectId: sub.objectId)
    }
}

extension CodableUpdate {
    init(from up: Update, className: String, scheme: MeerkatSchemeDescriptor) throws {
        try self.init(attribute: up.attribute, value: scheme.getData(for: up.attribute, value: up.value, in: className))
    }
}

extension CodableCreationDescription {
    init(from cd: CreationDescription, scheme: MeerkatSchemeDescriptor) throws {
        try self.init(groupID: cd.groupID, object: CodableSubtransaction(from: cd.object, scheme: scheme))
    }
}
