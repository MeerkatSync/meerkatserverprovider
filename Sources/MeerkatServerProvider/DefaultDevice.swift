//
//  DefaultDevice.swift
//  
//
//  Created by Filip Klembara on 10/03/2020.
//

public struct DefaultDevice: Device, Content {
    public let id: String
    public let description: String

    public init(id: String, description: String = "") {
        self.id = id
        self.description = description
    }
}
