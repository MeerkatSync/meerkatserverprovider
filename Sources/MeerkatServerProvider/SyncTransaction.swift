//
//  SyncTransaction.swift
//  
//
//  Created by Filip Klembara on 21/02/2020.
//

struct SyncTransaction: Transaction {
    let deletions: [SubTransaction]

    let creations: [CreationDescription]

    let modifications: [SubTransaction]
}

extension SyncTransaction {
    init(from transaction: CodableTransaction, scheme: MeerkatSchemeDescriptor) throws {
        self.deletions = try transaction.deletions.map { try SyncSubtransaction(from: $0, scheme: scheme) }
        self.creations = try transaction.creations.map { try SyncCreationDescription(from: $0, scheme: scheme) }
        self.modifications = try transaction.modifications.map { try SyncSubtransaction(from: $0, scheme: scheme) }
    }
}

struct SyncSubtransaction: SubTransaction {
    let timestamp: Date

    let updates: [Update]

    let className: String

    let objectId: ObjectID

    init(from sub: CodableSubtransaction, scheme: MeerkatSchemeDescriptor) throws {
        timestamp = sub.timestamp
        className = sub.className
        objectId = sub.objectId
        updates = try sub.updates.map { try SyncUpdate(from: $0, className: sub.className, scheme: scheme) }
    }
}

struct SyncCreationDescription: CreationDescription {
    let groupID: GroupID

    let object: SubTransaction

    init(from cd: CodableCreationDescription, scheme: MeerkatSchemeDescriptor) throws {
        groupID = cd.groupID
        object = try SyncSubtransaction(from: cd.object, scheme: scheme)
    }
}

struct SyncUpdate: Update {
    let attribute: String

    let value: UpdateValue

    init(from up: CodableUpdate, className: String, scheme: MeerkatSchemeDescriptor) throws {
        attribute = up.attribute
        value = try scheme.getValue(for: attribute, from: up.value, in: className)
    }
}
