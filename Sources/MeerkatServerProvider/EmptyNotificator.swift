//
//  EmptyNotificator.swift
//  
//
//  Created by Filip Klembara on 10/03/2020.
//

public final class EmptyNotificator: Notificators, ServiceType {
    public let notificatorDBWrapper: NotificatorDBWrapper

    public static func makeService(for container: Container) throws -> EmptyNotificator {
        let notDB = try container.make(NotificatorDBWrapper.self)
        return .init(notificatorDBWrapper: notDB, notificators: [])
    }

    public static var serviceSupports: [Any.Type] {
        [Notificators.self]
    }

    public let notificators: [Notificator]

    private init(notificatorDBWrapper: NotificatorDBWrapper, notificators: [Notificator] = []) {
        self.notificatorDBWrapper = notificatorDBWrapper
        self.notificators = notificators
    }
}

