// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatServerProvider",
    platforms: [
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatServerProvider",
            targets: ["MeerkatServerProvider"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatserversynccontroller", from: "1.0.0"),
        .package(url: "https://github.com/vapor/vapor", from: "3.0.0"),
        .package(url: "https://github.com/vapor/auth", from: "2.0.0"),
        .package(url: "https://gitlab.com/MeerkatSync/meerkatmysql", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatServerProvider",
            dependencies: ["Vapor", "MeerkatServerSyncController", "Authentication", "MeerkatMySQL"]),
        .testTarget(
            name: "MeerkatServerProviderTests",
            dependencies: ["MeerkatServerProvider"]),
    ]
)
